from django.urls import path
from tasks.views import task_create_view, task_list_view, task_edit_view

urlpatterns = [
    path("create/", task_create_view, name="create_task"),
    path("mine/", task_list_view, name="show_my_tasks"),
    path("edit/<int:id>", task_edit_view, name="edit_task"),
]
