from django import forms
from django.forms import ModelForm
from tasks.models import Task


class DateTimeInput(forms.DateTimeInput):
    input_type = "datetime-local"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ("is_completed",)
        widgets = {
            "start_date": DateTimeInput(),
            "due_date": DateTimeInput(),
        }


class TaskEditForm(ModelForm):
    class Meta:
        model = Task
        exclude = (
            "start_date",
            "project",
        )
        widgets = {
            "due_date": DateTimeInput(),
        }
