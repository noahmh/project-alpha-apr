from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, TaskEditForm
from tasks.models import Task


# Create your views here.
@login_required
def task_create_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def task_list_view(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)


@login_required
def task_edit_view(request, id):
    task = get_object_or_404(Task, id=id)
    if task.assignee != request.user:
        return redirect("show_my_tasks")
    else:
        if request.method == "POST":
            form = TaskEditForm(request.POST, instance=task)
            if form.is_valid():
                form.save()
                return redirect("show_my_tasks")
        else:
            form = TaskEditForm(instance=task)
        context = {
            "form": form,
        }
    return render(request, "tasks/edit.html", context)
