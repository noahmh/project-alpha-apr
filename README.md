## OVERVIEW
This is a project management application. It allows users to create projects and tasks related to a project. Projects can be shared by many users, with tasks within that project being assigned to a specific partner in the project.

## HOW TO RUN
1. Create a virtual environment
2. Activate, then install dependencies with pip install -r requirements
3. Connect to localhost:8000
